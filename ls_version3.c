/*
*  ls_version3.c:
*  
*  SIR!

*     Give "-l" , "-l somefile" as an argument and see what happens..!!!
*     UID , GID and time is set as you asked.
*/


#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>

extern int errno;
void do_ls(char*);
void show_stat_info(char*);
void uidtoname(int);
void gidtoname(int);
void permission(int);

int main(int argc, char* argv[]){
   if (argc == 1){
         printf("Directory listing of pwd:\n");
      do_ls(".");
   }
   else if(strcmp("-l" , argv[1])==0){
	if(argc > 3){
      		fprintf(stderr, "Incorrect number of arguments\n");
      	exit(1);
   	}
	else if(argc == 3){
      		show_stat_info(argv[2]);
   	}
	else if(argc == 2){
		   struct dirent * entry;
		   DIR * dp = opendir(".");
		   if (dp == NULL){
		      fprintf(stderr, "Cannot open directory:%s\n","");
		      return 0;
		   }
		   errno = 0;
		   while((entry = readdir(dp)) != NULL){
			 if(entry == NULL && errno != 0){
		  		perror("readdir failed");
				exit(1);
		  	}else{
				if(entry->d_name[0] == '.')
				    continue;
			}     	show_stat_info(entry->d_name);
		   }
		   closedir(dp);
      		
   	}
   }
   else{
      int i = 0;
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	 do_ls(argv[i]);
      }
   }
   
   return 0;
}
/*--------------------------------------------------------------------------------------------------------------------*/

void do_ls(char * dir)
{
   struct dirent * entry;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   errno = 0;
   while((entry = readdir(dp)) != NULL){
         if(entry == NULL && errno != 0){
  		perror("readdir failed");
		exit(1);
  	}else{
                if(entry->d_name[0] == '.')
                    continue;
        }     	printf("%s\n",entry->d_name);
   }
   closedir(dp);
}
/*--------------------------------------------------------------------------------------------------------------------*/
void show_stat_info(char *fname){
   struct stat info;
   int rv = lstat(fname, &info);
   if (rv == -1){
      perror("stat failed");
      exit(1);
   }
   permission(info.st_mode);
   printf("link count: %ld	", info.st_nlink);
   uidtoname(info.st_uid);
   gidtoname(info.st_gid);
   printf("size: %ld	", info.st_size);
   long secs = info.st_mtime;
   char str[strlen(ctime(&secs))];
   strcpy(str,ctime(&secs));
   str[strlen(ctime(&secs))-1]='0';
   printf("%s	", str);
   printf("%s	\n", fname );
}
/*--------------------------------------------------------------------------------------------------------------------*/
void uidtoname(int uid){
   struct passwd* pwd = getpwuid(uid);
   if (pwd == NULL){
      if (errno == 0)
         printf("Record not found in passwd file.\n");
      else
         perror("getpwuid failed");
   }
   else
       printf("%s	", pwd->pw_name);
}
/*--------------------------------------------------------------------------------------------------------------------*/
void gidtoname(int gid){
   struct group * grp = getgrgid(gid);
   errno = 0;
   if (grp == NULL){
      if (errno == 0)
          printf("Record not found in /etc/group file.\n");
      else
          perror("getgrgid failed");
   }else
      printf("%s	", grp->gr_name);
}
/*--------------------------------------------------------------------------------------------------------------------*/
void permission(int mode){
   char str[11];
   strcpy(str, "----------");
//filetype
   if((mode & 0170000) == 0010000) str[0] = 'p';
   if((mode & 0170000) == 0020000) str[0] = 'c';
   if((mode & 0170000) == 0040000) str[0] = 'd';
   if((mode & 0170000) == 0060000) str[0] = 'b';
   if((mode & 0170000) == 0120000) str[0] = 'l';
   if((mode & 0170000) == 0140000) str[0] = 's';
//owner  permissions
   if((mode & 0000400) == 0000400) str[1] = 'r';
   if((mode & 0000200) == 0000200) str[2] = 'w';
   if((mode & 0000100) == 0000100) str[3] = 'x';
//group permissions
   if((mode & 0000040) == 0000040) str[4] = 'r';
   if((mode & 0000020) == 0000020) str[5] = 'w';
   if((mode & 0000010) == 0000010) str[6] = 'x';
//others  permissions
   if((mode & 0000004) == 0000004) str[7] = 'r';
   if((mode & 0000002) == 0000002) str[8] = 'w';
   if((mode & 0000001) == 0000001) str[9] = 'x';
//special  permissions
   if((mode & 0004000) == 0004000) str[3] = 's';
   if((mode & 0002000) == 0002000) str[6] = 's';
   if((mode & 0001000) == 0001000) str[9] = 't';
   printf("%s	", str);
}